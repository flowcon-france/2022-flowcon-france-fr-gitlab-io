import fetch from 'node-fetch';
import { load } from 'cheerio';
import fs from 'fs';
import sharp from 'sharp';
import Jimp from 'jimp';


const url = 'https://flowcon22.sched.com';

const siteResponse = await fetch(url);
const siteBody = await siteResponse.text();

const site = load(siteBody);

const buildImageName = (speakerName) => 
    speakerName
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/\s+/g, '-')
        .toLowerCase();

const resizer =
    sharp()
        .resize(256, 256)
        .jpeg()

const downloadImage = async (speaker) => {
    if (!speaker.imageUrl) {
        console.log('missing image: ' + JSON.stringify(speaker));
        return;
    }
    const imageUrl = 'http:' + speaker.imageUrl;
    const fileName = `./result/images/${speaker.imageName}.jpg`;
    Jimp.read(imageUrl, (err, img) => {
        if (err) throw err;
        img
          .resize(256, 256)
          .quality(60)
          .write(fileName);
      });
}

async function loadSession(elmt) {
    const speakers = [];
    const relativeSessionUrl = site(elmt).children('a').attr('href');
    const sessionEventUrl = url + '/' + relativeSessionUrl;

    const sessionResponse = await fetch(sessionEventUrl);
    const sessionBody = await sessionResponse.text();
    const sessionPage = load(sessionBody);
    const title = sessionPage('.event > a[href="#"]').text();

    sessionPage('.sched-person').each(async function(index, value) {
        const speakerName = sessionPage(value).find('h2').text();
        const imageUrl = sessionPage(value).find('a img').attr('src');

        const speaker = {
            name: speakerName,
            title: title.trim(),
            sessionUrl: sessionEventUrl,
            imageName: buildImageName(speakerName),
            imageUrl: imageUrl
        };

        await downloadImage(speaker);
        
        speakers.push(speaker);
    });
    return speakers;
}

async function loadSpeakers() {
    const speakerPromises = [];
    site('.event').each(function(_, elmt) {
        speakerPromises.push(loadSession(elmt));
    });
    const speakers = await Promise.all(speakerPromises);
    return speakers.flat();
}

const buildTomlSpeakerList = async () => {
    const speakers = await loadSpeakers();
    let toml = '';
    speakers.forEach(speaker => {
        toml = toml + '[[params.speakers]]\n';
        toml = toml + `    name = "${speaker.name}"\n`;
        toml = toml + `    talk = "${speaker.title}"\n`;
        toml = toml + `    photo = "${speaker.imageName}"\n`;
        toml = toml + `    url = "${speaker.sessionUrl}"\n`;
        toml = toml + '\n';
    });
    fs.writeFile('./result/speakers.toml', toml, function (err) {
        if (err) return console.log(err);
    });
}

buildTomlSpeakerList();
